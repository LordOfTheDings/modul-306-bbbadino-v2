import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Dealer implements Person{
    private List<Card> pack = new ArrayList<>();
    private List<Card> deckOfCards = new ArrayList<>();
    private int bankAccount;

    public int getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(int bankAccount) {
        this.bankAccount = bankAccount;
    }

    public void setPack(List<Card> pack) {
        this.pack = pack;
    }

    public void collectBet(List<Integer> bets){
        for (int bet : bets) {
            bankAccount += bet;
        }
    }

    public void giveMoney(double money) {
        bankAccount -= money;
    }

    // deal the first card of the pack
    public Card dealCard() {
        Card card = pack.get(0);
        pack.remove(card);
        return card;
    }

    public void shufflePack() {
        Collections.shuffle(pack);
    }

    public void drawCard(){
        deckOfCards.add(dealCard());
    }

    @Override
    public int revealDeck() {
        int score = 0;

        for (Card card: deckOfCards) {
            score += card.getScore();
        }

        // score has to be > 17
        while (score <= 17){
            Card drawnCard = pack.get(0);
            deckOfCards.add(drawnCard);
            pack.remove(drawnCard);
            score += drawnCard.getScore();
        }

        return score;
    }
}
