import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DealerTest {
    private Dealer dealer;
    public DealerTest(){
        dealer = new Dealer();
    }

    @org.junit.jupiter.api.Test
    public void testBankAccount(){
        dealer.setBankAccount(30000);
        assertEquals(30000, dealer.getBankAccount());
    }

    @org.junit.jupiter.api.Test
    public void testCollectBet(){
        List<Integer> bets = new ArrayList<>();
        bets.add(100);
        bets.add(150);
        bets.add(300);

        dealer.collectBet(bets);
        assertEquals(550, dealer.getBankAccount());
    }

    @org.junit.jupiter.api.Test
    public void testCollectBetNegative(){
        List<Integer> bets = new ArrayList<>();
        bets.add(1000);
        bets.add(-4000);
        bets.add(-100);

        dealer.collectBet(bets);
        assertEquals(-3100, dealer.getBankAccount());
    }

    @org.junit.jupiter.api.Test
    public void testGiveMoney(){
        dealer.giveMoney(300);
        assertEquals(-300, dealer.getBankAccount());
    }

    @org.junit.jupiter.api.Test
    public void testDealCard(){
        List<Card> cards = new ArrayList<>();
        cards.add(new Card(10));
        cards.add(new Card(9));

        dealer.setPack(cards);
        dealer.drawCard();
        dealer.drawCard();

        assertEquals(19, dealer.revealDeck());
    }

    @org.junit.jupiter.api.Test
    public void testRevealCard_Below18(){
        List<Card> cards = new ArrayList<>();
        cards.add(new Card(3));
        cards.add(new Card(8));
        cards.add(new Card(4));
        cards.add(new Card(8));

        dealer.setPack(cards);
        dealer.drawCard();
        dealer.drawCard();

        assertEquals(23, dealer.revealDeck());
    }
}