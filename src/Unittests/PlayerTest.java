import static org.junit.jupiter.api.Assertions.*;

class PlayerTest {
    private Player player;

    public PlayerTest(){
        player = new Player();
    }

    @org.junit.jupiter.api.Test
    public void testPlaceBet(){
        player.setPlacedBet(500);

        assertEquals(500, player.getPlacedBet());
    }

    @org.junit.jupiter.api.Test
    public void testBankAccount(){
        player.setBankAccount(30000);

        assertEquals(30000, player.getBankAccount());
    }

    @org.junit.jupiter.api.Test
    public void testRevealDeck(){
        player.hit(new Card(11));
        player.hit(new Card(8));

        assertEquals(19, player.revealDeck());
    }

    @org.junit.jupiter.api.Test
    public void testReceiveMoney(){
        player.receiveMoney(1000);

        assertEquals(1000, player.getBankAccount());
    }
}