import java.util.ArrayList;
import java.util.List;

public class Player implements Person {
    private int placedBet;
    private List<Card> deckOfCards = new ArrayList<>();
    private double bankAccount;

    public int getPlacedBet() {
        return placedBet;
    }

    public void setPlacedBet(int placedBet) {
        this.placedBet = placedBet;
    }

    // take a card
    public void hit(Card card) {
        deckOfCards.add(card);
    }

    // decline further cards
    public Integer stand(){
        return revealDeck();
    }

    public double getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(double bankAccount) {
        this.bankAccount = bankAccount;
    }

    public void receiveMoney(double money){
        bankAccount += money;
    }

    @Override
    public int revealDeck() {
        int score = 0;

        for (Card card: deckOfCards) {
            score += card.getScore();
        }

        return score;
    }
}
