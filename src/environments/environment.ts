// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAC6G9-LMr2CTFiV5RHYkxS2X03UjlkclQ",
    authDomain: "bbbadino-modul306-backend.firebaseapp.com",
    databaseURL: "https://bbbadino-modul306-backend.firebaseio.com",
    projectId: "bbbadino-modul306-backend",
    storageBucket: "bbbadino-modul306-backend.appspot.com",
    messagingSenderId: "898148845050",
    appId: "1:898148845050:web:491ba06bc13be014e35043",
    measurementId: "G-H4NEVBYBZV"
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
