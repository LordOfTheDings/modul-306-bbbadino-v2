import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AuthenticationService} from "./shared/services/authentication.service";
import {User} from "./shared/model/user";
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {AddJetonsDialogComponent} from "./add-jetons-dialog/add-jetons-dialog.component";
import {TransactionService} from "./shared/services/transaction.service";
import {AddCreditDialogComponent} from "./add-credit-dialog/add-credit-dialog.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'BBBadino';
  currentUser: User;

  constructor(
    private router: Router,
    public dialog: MatDialog,
    private authenticationService: AuthenticationService,
    private transactionService: TransactionService
  ) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  ngOnInit() {
  }

  isActive(path: string): boolean {
    return this.router.url == path;
  }

  logout() {
    this.authenticationService.logout();
  }

  async addJetons() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.data = this.currentUser.credit;

    const dialogRef = this.dialog.open(AddJetonsDialogComponent, dialogConfig);
    await dialogRef.afterClosed().toPromise().then(
      amount => {
        if (amount == undefined) {
          return;
        }
        this.transactionService.addJetons(amount);
      },
      err => {
        alert("error");
      }
    );
  }

  async depositMoney() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;

    const dialogRef = this.dialog.open(AddCreditDialogComponent, dialogConfig);
    await dialogRef.afterClosed().toPromise().then(
      amount => {
        if (amount == undefined) {
          return;
        }
        this.transactionService.addCredit(amount);
      },
      err => {
        alert("error");
      }
    );
  }
}
