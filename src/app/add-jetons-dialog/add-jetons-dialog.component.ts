import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-add-jetons-dialog',
  templateUrl: './add-jetons-dialog.component.html',
})
export class AddJetonsDialogComponent implements OnInit {
  currentCredit: number = 0;
  amountForm: FormGroup;
  submitted: boolean = false;

  constructor(private formBuilder: FormBuilder,
              private dialogRef: MatDialogRef<AddJetonsDialogComponent>,
              @Inject(MAT_DIALOG_DATA) credit) {
    this.currentCredit = credit;
  }

  ngOnInit(): void {
    this.amountForm = this.formBuilder.group({
      amount: [0, Validators.required],
    })
  }

  get f() {
    return this.amountForm.controls;
  }

  checkAmount(): boolean {
    return this.f.amount.value * 10 > this.currentCredit;
  }

  onSubmit() {
    this.submitted = true;
    if (this.checkAmount() || this.amountForm.invalid) {
      return;
    }
    this.dialogRef.close(this.f.amount.value);
  }
}
