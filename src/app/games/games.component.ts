import { Component, OnInit } from '@angular/core';

interface Game {
  name: string;
  urlPath: string;
}

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.css']
})
export class GamesComponent implements OnInit {
  games: Game[] = [];

  constructor() { }

  ngOnInit(): void {
    this.games.push({name: 'Blackjack', urlPath: 'blackjack'});
  }
}
