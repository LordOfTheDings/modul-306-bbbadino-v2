import {Component, OnInit} from '@angular/core';
import {User} from '../shared/model/user';
import {ApiService} from '../shared/services/api.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthGuard} from "../shared/auth.guard";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  user: User = {
    password: '',
    username: '',
    jetons: 0,
    credit: 0,
  }
  loading = false;
  submitted = false;
  registrationForm: FormGroup

  constructor(
    private apiService: ApiService,
    private builder: FormBuilder,
    private router: Router,
    guard: AuthGuard,
    route: ActivatedRoute) {
    guard.canActivate(route.snapshot, router.routerState.snapshot);
  }

  ngOnInit(): void {
    this.registrationForm = this.builder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  get f() {
    return this.registrationForm.controls;
  }

  async onSubmit() {
    this.submitted = true;

    if (this.registrationForm.invalid) {
      return;
    }

    this.loading = true;
    this.user.password = this.f.password.value;
    this.user.username = this.f.username.value;
    if (this.user.password === '' || this.user.username === '') {
      return;
    }
    await this.apiService.addUser(this.user);
    this.router.navigateByUrl("signin");
  }
}
