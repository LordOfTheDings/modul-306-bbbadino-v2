import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BlackjackComponent} from './blackjack.component';
import {AuthenticationService} from "../shared/services/authentication.service";
import {TransactionService} from "../shared/services/transaction.service";
import {AngularFireDatabaseModule} from "@angular/fire/database";
import {AngularFireModule} from "@angular/fire";
import {environment} from "../../environments/environment";
import {Router} from "@angular/router";

describe('BlackjackComponent', () => {
  let component: BlackjackComponent;
  let fixture: ComponentFixture<BlackjackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireDatabaseModule
      ],
      declarations: [BlackjackComponent],
      providers: [
        AuthenticationService,
        TransactionService,
        Router
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlackjackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call service when giveMoney is called', () => {
    component.giveMoney(1);

    expect(transactionService.jetonsWon).toHaveBeenCalled();
  });

  it('should be placed when bet is placed', () => {
    component.placedBet = 1;
    component.isBetPlaced();

    expect(component.betPlaced).toBeTrue();
  });

  it('should splitcard', () => {
    component.splitCard();

    expect(component.deckOfCardsPlayer).toBe(component.deckOfCardsPlayerSplit);
  });

  it('should be false', () => {
    const splitAvailable = component.isSplitAvailable();

    expect(splitAvailable).toBeFalse();
  });
});
