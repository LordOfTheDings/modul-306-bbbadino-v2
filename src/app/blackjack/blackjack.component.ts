import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from "../shared/services/authentication.service";
import {TransactionService} from "../shared/services/transaction.service";
import {User} from "../shared/model/user";
import {Player} from "../shared/model/player";
import {Dealer} from "../shared/model/dealer";

@Component({
  selector: 'app-blackjack',
  templateUrl: './blackjack.component.html',
  styleUrls: ['./blackjack.component.css']
})
export class BlackjackComponent implements OnInit {
  player: Player;
  dealer: Dealer;
  pack: number[] = [];

  betPlaced: boolean = false;
  lockBet: boolean = false;
  doubledDown: boolean = false;

  showDealerCard: boolean = false;
  isSplitted: boolean = false;
  keepPlaying = true;

  user: User;

  constructor(
    private readonly authenticationService: AuthenticationService,
    private readonly transactionService: TransactionService
    ) {
    this.player = new Player();
    this.dealer = new Dealer();
    this.authenticationService.currentUser.subscribe(x => {
      this.user = x;
    });
  }

  ngOnInit(): void {
    this.addStandardPack();
  }

  shufflePack() {
    this.shuffle(this.pack);
  }

  playRound(takeCard: boolean): void {
    this.showDealerCard = !takeCard;

    if (this.keepPlaying) {
      this.lockBet = true;

      if (takeCard) {
        if(this.player.isFirstRound()){
            this.player.drawCard((this.drawCard()));
        }
        this.player.drawCard((this.drawCard()));
        if (this.isSplitted) {
          this.player.drawCard((this.drawCard()));
        }
      }
      if (this.dealer.getScore() < 17) {
        if(this.dealer.isFirstRound()){
          this.dealer.drawCard(this.drawCard());
        }
        this.dealer.drawCard(this.drawCard());
      }
      this.checkForGameEnding();
    }
  }

  drawCard() {
    const card = this.dealCard();
    this.shufflePack();
    return card;
  }

  setBet(amountEvent: any): void {
    const amount = amountEvent?.srcElement?.value;
    if (amount && amount > 0 && amount <= this.user.jetons) {
      this.player.placedBet = amount;
    }

    this.isBetPlaced();
  }

  isBetPlaced(): void {
    this.betPlaced = this.player.placedBet > 0;
  }

  getScore(deckOf: number[]): number {
    let score = 0;

    deckOf.forEach(x => {
      score += x;
    })

    return score;
  }

  splitCard() {
    if (!this.isSplitted) {
      this.player.splitDeck();
      this.player.drawCard(this.drawCard());
      this.player.drawCard(this.drawCard());
    }

    this.isSplitted = true;
  }

  isSplitAvailable(): boolean {
    return this.player.isSplitAvailable() &&
      !this.isSplitted &&
      this.user.jetons >= this.player.placedBet * 2;
  }

  isDoubleDownAvailable(): boolean {
    if(this.doubledDown){
      return false;
    }
    return this.player.isDoubleDownAvailable(this.user.jetons);
  }

  private shuffle(array) {
    array.sort(() => Math.random() - 0.5);
  }

  private dealCard(): number {
    if (this.pack.length != 0) {
      const card = this.pack[0];
      this.pack.splice(0, 1);
      return card;
    }
  }

  private addStandardPack() {
    for (let i = 0; i < 4; i++) {
      this.pack.push(2);
      this.pack.push(3);
      this.pack.push(4);
      this.pack.push(5);
      this.pack.push(6);
      this.pack.push(7);
      this.pack.push(8);
      this.pack.push(9);
      this.pack.push(10);
      this.pack.push(11);
    }

    this.shufflePack();
  }

  doubleDown() {
    this.player.placedBet = +this.player.placedBet * +2;
    this.player.drawCard(this.drawCard());
    if(this.isSplitted){
      this.player.drawCard(this.drawCard());
    }
    this.doubledDown = true;
    this.checkForGameEnding();
  }

  private checkForGameEnding() {
    if (this.player.getFirstHand() >= 21 || this.player.getSecondHand() >= 21) {
      this.keepPlaying = false;

      const scorePlayerFirstHand = this.player.getFirstHand();
      const scorePlayerSecondHand = this.player.getSecondHand();
      const scoreBank = this.dealer.getScore();

      if (scorePlayerFirstHand <= 21 && scorePlayerFirstHand > scoreBank || (scoreBank > 21 && scorePlayerFirstHand < 22)) {
        this.transactionService.jetonsWon(this.player.placedBet);
      } else {
        this.transactionService.jetonsWon((-1 * this.player.placedBet));
      }

      if (this.isSplitted && (scorePlayerSecondHand <= 21 && scorePlayerSecondHand > scoreBank || (scoreBank > 21 && scorePlayerSecondHand < 22))) {
        this.transactionService.jetonsWon((-1 * this.player.placedBet));
      } else if(this.isSplitted) {
        this.transactionService.jetonsWon((-1 * this.player.placedBet));
      }
    }
  }
}
