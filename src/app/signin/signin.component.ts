import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthenticationService} from "../shared/services/authentication.service";
import {AuthGuard} from "../shared/auth.guard";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  loading: boolean = false;
  error: boolean = false;
  submitted: boolean = false;
  signinForm: FormGroup;

  constructor(
    private authService: AuthenticationService,
    private  builder: FormBuilder,
    private router: Router,
    guard: AuthGuard,
    route: ActivatedRoute) {
    guard.canActivate(route.snapshot, router.routerState.snapshot);
  }

  get f() {
    return this.signinForm.controls;
  }

  ngOnInit(): void {
    this.signinForm = this.builder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  login() {
    this.submitted = true;
    if (this.signinForm.invalid) {
      return;
    }

    this.loading = true;
    this.authService.login(this.f.username.value, this.f.password.value).subscribe(
      res => {
        this.error = res;
        if(this.router.url != "/blackjack"){
          this.router.navigateByUrl("games");
        }
      },
      err => {

      }
    );
    this.loading = false;
  }

}
