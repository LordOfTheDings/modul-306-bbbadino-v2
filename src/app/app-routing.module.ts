import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RegistrationComponent} from './registration/registration.component';
import { SigninComponent } from './signin/signin.component';
import {GamesComponent} from "./games/games.component";
import {BlackjackComponent} from "./blackjack/blackjack.component";


const routes: Routes = [
    {
      path: 'signin',
      component: SigninComponent
    },
    {
      path: 'registration',
      component: RegistrationComponent
    },
    {
      path: 'blackjack',
      component: BlackjackComponent
    },
    {
      path: 'games',
      component: GamesComponent,
      children: [
        {
          path: 'blackjack',
          redirectTo: 'blackjack'
        }
      ]
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
