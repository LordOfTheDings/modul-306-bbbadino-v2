import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {environment} from '../environments/environment';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {AngularFireModule} from '@angular/fire';
import {ApiService} from './shared/services/api.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RegistrationComponent} from './registration/registration.component';
import {BlackjackComponent} from './blackjack/blackjack.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule} from '@angular/material/dialog';
import {MatSliderModule} from '@angular/material/slider';
import {SigninComponent} from './signin/signin.component';
import {AddJetonsDialogComponent} from './add-jetons-dialog/add-jetons-dialog.component';
import {AddCreditDialogComponent} from './add-credit-dialog/add-credit-dialog.component';
import { GamesComponent } from './games/games.component';
import {TransactionService} from "./shared/services/transaction.service";
import {AuthenticationService} from "./shared/services/authentication.service";
import {PayoutBuilder} from "./shared/payout-builder";

@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    RegistrationComponent,
    BlackjackComponent,
    AddJetonsDialogComponent,
    AddCreditDialogComponent,
    GamesComponent,
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MatDialogModule,
    MatSliderModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
  ],
  providers: [
    ApiService,
    TransactionService,
    AuthenticationService,
    PayoutBuilder,
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false}}
  ],
  bootstrap: [AppComponent],
  entryComponents: [AppComponent, AddJetonsDialogComponent]
})
export class AppModule {
}
