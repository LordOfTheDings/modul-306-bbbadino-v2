import {User} from "./model/user";

export class PayoutBuilder {
  private credit: number = 0;
  private jetons: number = 0;
  private user: User;

  build(){
    this.user.credit = this.credit;
    this.user.jetons = this.jetons;
    return this.user;
  }

  startPayoutTransaction(user: User){
    this.user = user;
    this.jetons = this.user.jetons;
    this.credit = this.user.credit;
    return this;
  }

  addWonJetons(amount: number){
    this.jetons = +this.user.jetons + +amount;
    return this;
  }

  addJetons(amount: number){
    this.jetons = +this.user.jetons + +amount;
    this.addCredit(-(amount * 10));
    return this;
  }

  takeJetons(amount: number){
    this.jetons = +this.user.jetons + -amount;
    return this;
  }

  addCredit(amount: number){
    this.credit = +this.user.credit+ +amount;
    return this;
  }
}
