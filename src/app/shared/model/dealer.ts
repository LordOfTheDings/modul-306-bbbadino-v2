import {Person} from "./person";

export class Dealer extends Person {
  cards: number[] = [];

  drawCard(card: number) {
    this.cards.push(card);

  }

  getScore() {
    let score = 0;
    this.cards.forEach(x => {
      score += x;
    })
    return score;
  }

  isFirstRound(): boolean {
    return this.cards.length == 0;
  }


  revealDeck(): number[] {
    return this.cards;
  }
}
