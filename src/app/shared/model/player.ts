import {Person} from "./person";

export class Player extends Person {

  private firstHand: number[] = [];
  private secondHand: number[] = [];
  placedBet: number = 0;
  private doubledDown: boolean = false;

  constructor(firstHand?: number[], secondHand?: number[]) {
    super();
    if(firstHand == undefined || secondHand == undefined){
      return;
    }
    this.secondHand = secondHand;
    this.firstHand = firstHand;
  }

  drawCard(card: number): void {
    if(card == 11 && (this.getFirstHand() > 21 || this.getSecondHand()+card > 21)){
      card = 1;
    }

    if(this.secondHand.length > 0){
      if(this.firstHand < this.secondHand){
        this.firstHand.push(card);
      }else{
        this.secondHand.push(card)
      }
    }else{
      this.firstHand.push(card);
    }
  }

  isSplitAvailable(){
    return this.firstHand.length ==2 && this.firstHand[0] == this.firstHand[1];
  }

  isDoubleDownAvailable(jetons: number){
   let firstTwoValues = this.firstHand[0]+this.firstHand[1];
    if(this.secondHand.length >0){
      this.doubledDown =  this.placedBet*4 <= jetons && (firstTwoValues >=9 && firstTwoValues <=11);
    }
    this.doubledDown =  this.placedBet*2 <= jetons && (firstTwoValues >=9 && firstTwoValues <=11) ;
    return this.doubledDown;
  }

  isFirstRound(): boolean {
    return this.firstHand.length == 0;
  }

  splitDeck(){
    this.secondHand.push(this.firstHand[1]);
    this.firstHand.splice(1,1);
  }

  getFirstHand(){
    let score = 0;

    this.firstHand.forEach(x => {
      score += x;
    })

    return score;
  }

  getSecondHand(){
    let score = 0;

    this.secondHand.forEach(x => {
      score += x;
    })

    return score;
  }

  revealDeck(): number[] {
    return this.firstHand;
  }

  //if cards were splitted
  revealSecondDeck():number[]{
    return this.secondHand;
  }





}
