export abstract class Person {

  abstract drawCard(card: number): void;
  abstract revealDeck(): number[];
  abstract isFirstRound():boolean;

}
