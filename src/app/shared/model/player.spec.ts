import { Player } from './player';

describe('Player', () => {
  it('should create an instance', () => {
    expect(new Player()).toBeTruthy();
  });

  it('should draw card', () => {
    const player = new Player();

    const returnValue = player.drawCard(1);

    expect(player.revealDeck().length).toBe(1);
  });

  it('should split cards', () => {
    let cards = [0,1];
    const player = new Player(cards);

    player.splitDeck();

    expect(player.getFirstHand()).toBe([0]);
  });

  it('should return false', () => {
    const player = new Player();

    const returnValue = player.isSplitAvailable();

    expect(returnValue).toBeFalse();

  });

  it('should return true', () => {
    let cards = [1,1];
    const player = new Player(cards);

    const returnValue = player.isSplitAvailable();

    expect(returnValue).toBeTrue();
  });

  it('should return false', () => {
    let cards = [1,10];
    const player = new Player(cards);
    player.placedBet = 0;

    const returnValue = player.isDoubleDownAvailable(1);

    expect(returnValue).toBeFalse();
  });

  it('should return true ', () => {
    let cards = [1,10];
    const player = new Player(cards);
    player.placedBet = 2;
    const returnValue = player.isDoubleDownAvailable(1);

    expect(returnValue).toBeTrue();
  });

  it('should return true ', () => {

    const player = new Player();
    player.placedBet = 2;
    const returnValue = player.isFirstRound();

    expect(returnValue).toBeTrue();
  });

  it('should return second deck ', () => {
    let cards = [1,10];
    const player = new Player(cards);
    player.placedBet = 2;
    player.splitDeck();
    const returnValue = player.revealSecondDeck();
    expect(returnValue).toEqual([10]);
  });
});
