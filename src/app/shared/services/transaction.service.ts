import {Injectable} from '@angular/core';
import {ApiService} from "./api.service";
import {User} from "../model/user";
import {AuthenticationService} from "./authentication.service";
import {PayoutBuilder} from "../payout-builder";

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  user: User={};
  constructor(private apiService: ApiService,
              private authService: AuthenticationService,
              private payoutBuilder: PayoutBuilder,
  ) {
    this.authService.currentUser.subscribe(x => this.user = x);
  }

  jetonsWon(amount: number) {
    let payedUser;
    if(amount <0){
      payedUser = this.payoutBuilder.startPayoutTransaction(this.user).takeJetons(-(amount)).build();
    }else {
      payedUser = this.payoutBuilder.startPayoutTransaction(this.user).addWonJetons(amount).build();
    }
    this.apiService.updateUser(payedUser);
    this.authService.setCurrentUserValue(payedUser);
  }

  addJetons(amount: number) {
    let payedUser = this.payoutBuilder.startPayoutTransaction(this.user).addJetons(amount).build();
    this.apiService.updateUser(payedUser);
    this.authService.setCurrentUserValue(payedUser);
  }

  addCredit(amount: number) {
    let payedUser = this.payoutBuilder.startPayoutTransaction(this.user).addCredit(amount).build();
    this.apiService.updateUser(payedUser);
    this.authService.setCurrentUserValue(payedUser);
  }
}
