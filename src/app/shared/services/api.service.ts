import {Injectable} from '@angular/core';
import {User} from '../model/user';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {TransactionService} from "./transaction.service";

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private dbPath = 'users';
  usersCollection: AngularFirestoreCollection<User>;
  users: Observable<User[]>;

  constructor(private afs: AngularFirestore) {
    this.usersCollection = this.afs.collection(this.dbPath, ref => ref.orderBy('username', 'asc'));
    this.users = this.usersCollection.snapshotChanges().pipe(map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data() as User;
        data.id = a.payload.doc.id;
        return data;
      });
    }));
  }

  getAllUsers(): Observable<User[]> {
    return this.users;
  }

  addUser(user: User): Promise<User> {
    return this.afs.collection(this.dbPath).add({...user});
  }

  updateUser(user: User) {
    let userDoc = this.afs.doc(`users/${user.id}`);
    userDoc.update({...user});
  }
}
