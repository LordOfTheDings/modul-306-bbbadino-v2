import {Injectable} from '@angular/core';
import {ApiService} from "./api.service";
import {User} from "../model/user";
import {BehaviorSubject, Observable} from "rxjs";
import {Router} from "@angular/router";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(private apiService: ApiService, private router: Router) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(sessionStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  logout() {
    sessionStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
    this.router.navigateByUrl("signin");
  }

  login(username: string, password: string): Observable<boolean> {
    let users: User[] = null;
    let rejected = true;

    return this.apiService.getAllUsers().pipe(map(
      res => {
        users = res;
        for (let i = 0; i < users.length; ++i) {
          if (users[i].password == password && users[i].username == username) {
            this.setCurrentUserValue(users[i]);
            rejected = false;
            return rejected;
          }
        }
        return rejected;
      }));
  }

  setCurrentUserValue(user: User){
    sessionStorage.setItem('currentUser', JSON.stringify(user));
    this.currentUserSubject.next(user);
  }

  getCurrentUserValue(): User {
    return this.currentUserSubject.value;
  }
}
