import java.util.List;

public class Blackjack {
    private List<Player> players;
    private Dealer dealer = new Dealer();
    private int activeBets;
    private boolean isRoundRunning;
    private Player currentPlayer;
    private int currentPlayerIndex = 0;

    public Blackjack() {
        currentPlayer = players.get(currentPlayerIndex);
    }

    public void startRound(){

        // deal two cards each
        for (int i = 0; i < 2; i++ ){
            for (Player player : players) {
                Card card = dealer.dealCard();
                player.hit(card);
                dealer.drawCard();
            }
        }

        for (Player player : players) {
            requestBet();
            nextPlayer();
        }

        while (isRoundRunning){
            requestAction();
            nextPlayer();
        }
    }

    // move to the next player
    private void nextPlayer(){
        currentPlayerIndex++;

        if(currentPlayer.equals(dealer)) {
            currentPlayerIndex = 0;
        }
        if (currentPlayerIndex >= players.size()) {
            currentPlayerIndex = -1;
        }

        if (currentPlayerIndex < 0){
            //TODO currentPlayer = dealer;
        }

        currentPlayer = players.get(currentPlayerIndex);
    }

    // request bet from user
    public String requestBet() {
        return "Please place a bet";
    }

    // request bet from user
    public String requestAction() {
        return "Please take/reject card";
    }

    // hit/stand input from user
    public void cardAction(boolean takeCard) {
        if (takeCard)
        {
            currentPlayer.hit(dealer.dealCard());
            return;
        }

        int score = currentPlayer.stand();
        int scoreDealer = dealer.revealDeck();

        if ((score < 21) && (score > scoreDealer))
        {
            // player won
            double profit = currentPlayer.getPlacedBet() * 1.5;
            dealer.giveMoney(profit);
            currentPlayer.receiveMoney(profit);
        }

        isRoundRunning = false;
    }

    //place bet input from user
    public void placeBet(int bet){
        double currentBankAccount = currentPlayer.getBankAccount();
        currentPlayer.setBankAccount(currentBankAccount - bet);
        currentPlayer.setPlacedBet(bet);

        activeBets += bet;
    }

    //quit input from user
    public void quit(){
        isRoundRunning = false;
    }
}
